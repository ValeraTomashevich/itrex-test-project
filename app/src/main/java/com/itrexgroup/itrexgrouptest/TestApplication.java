package com.itrexgroup.itrexgrouptest;

import android.app.Application;

import com.itrexgroup.itrexgrouptest.data.DataManager;
import com.itrexgroup.itrexgrouptest.di.component.ApplicationComponent;
import com.itrexgroup.itrexgrouptest.di.component.DaggerApplicationComponent;
import com.itrexgroup.itrexgrouptest.di.module.ApplicationModule;

import javax.inject.Inject;

public class TestApplication extends Application {

    @Inject
    DataManager dataManager;

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();

        applicationComponent.inject(this);
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
