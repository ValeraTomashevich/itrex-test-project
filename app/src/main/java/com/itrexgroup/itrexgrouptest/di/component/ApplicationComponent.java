package com.itrexgroup.itrexgrouptest.di.component;

import android.content.Context;

import com.itrexgroup.itrexgrouptest.TestApplication;
import com.itrexgroup.itrexgrouptest.data.DataManager;
import com.itrexgroup.itrexgrouptest.di.ApplicationContext;
import com.itrexgroup.itrexgrouptest.di.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(TestApplication application);

    @ApplicationContext
    Context context();

    DataManager getDataManager();
}
