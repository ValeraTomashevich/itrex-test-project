package com.itrexgroup.itrexgrouptest.di.module;

import android.content.Context;

import com.itrexgroup.itrexgrouptest.TestApplication;
import com.itrexgroup.itrexgrouptest.data.AppDataManager;
import com.itrexgroup.itrexgrouptest.data.DataManager;
import com.itrexgroup.itrexgrouptest.di.ApplicationContext;
import com.itrexgroup.itrexgrouptest.service.ApiHelper;
import com.itrexgroup.itrexgrouptest.service.AppApiHelper;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {
    private final TestApplication application;

    public ApplicationModule(TestApplication application) {
        this.application = application;
    }

    @Provides
    @ApplicationContext
    TestApplication provideApplication() {
        return application;
    }

    @Provides
    Context provideContext() {
        return application;
    }

    @Provides
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

    @Provides
    ApiHelper provideApiHelper(AppApiHelper appApiHelper) {
        return appApiHelper;
    }
}
