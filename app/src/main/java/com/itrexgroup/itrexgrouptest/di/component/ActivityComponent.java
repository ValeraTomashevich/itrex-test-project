package com.itrexgroup.itrexgrouptest.di.component;

import com.itrexgroup.itrexgrouptest.di.PerActivity;
import com.itrexgroup.itrexgrouptest.di.module.ActivityModule;
import com.itrexgroup.itrexgrouptest.ui.movie.MovieActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MovieActivity activity);
}
