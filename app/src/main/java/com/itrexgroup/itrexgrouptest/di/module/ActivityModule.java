package com.itrexgroup.itrexgrouptest.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.itrexgroup.itrexgrouptest.di.ActivityContext;
import com.itrexgroup.itrexgrouptest.di.PerActivity;
import com.itrexgroup.itrexgrouptest.ui.movie.MovieMvpPresenter;
import com.itrexgroup.itrexgrouptest.ui.movie.MovieMvpView;
import com.itrexgroup.itrexgrouptest.ui.movie.MoviePresenter;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class ActivityModule {

    private final AppCompatActivity activity;

    public ActivityModule(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return activity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return activity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    @PerActivity
    MovieMvpPresenter<MovieMvpView> provideMovieMvpPresenter(MoviePresenter<MovieMvpView> presenter) {
        return presenter;
    }
}
