package com.itrexgroup.itrexgrouptest.data;

import android.content.Context;

import com.itrexgroup.itrexgrouptest.service.ApiHelper;

import javax.inject.Inject;

public class AppDataManager implements DataManager {

    private final Context context;
    private final ApiHelper apiHelper;

    @Inject
    public AppDataManager(Context context, ApiHelper apiHelper) {
        this.context = context;
        this.apiHelper = apiHelper;
    }
}
