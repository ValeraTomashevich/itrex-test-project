package com.itrexgroup.itrexgrouptest.data;

/**
 * DataManager: It is an interface that is implemented by the AppDataManager.
 * It contains methods, exposed for all the data handling operations.
 * Ideally, it delegates the services provided by all the Helper classes.
 * For this DataManager interface extends DbHelper, PreferenceHelper and ApiHelper interfaces.
 */
public interface DataManager {
}
