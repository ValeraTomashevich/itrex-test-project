package com.itrexgroup.itrexgrouptest.ui.base;

/**
 * MvpPresenter: It is an interface, that is implemented by the Presenter.
 * It contains methods that are exposed to its View for the communication.
 *
 * @param <V> extends {@link MvpView}
 */
public interface MvpPresenter<V extends MvpView> {

    void onAttach(V mvpView);

    void onDetach();
}
