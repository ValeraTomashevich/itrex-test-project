package com.itrexgroup.itrexgrouptest.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.itrexgroup.itrexgrouptest.TestApplication;
import com.itrexgroup.itrexgrouptest.di.component.ActivityComponent;
import com.itrexgroup.itrexgrouptest.di.component.DaggerActivityComponent;
import com.itrexgroup.itrexgrouptest.di.module.ActivityModule;

public class BaseActivity extends AppCompatActivity implements MvpView {

    private ActivityComponent activityComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(((TestApplication) getApplication()).getApplicationComponent())
                .build();
    }

    public ActivityComponent getActivityComponent() {
        return activityComponent;
    }

    @Override
    public void showLoading() {
        hideLoading();
    }

    @Override
    public void hideLoading() {

    }
}
