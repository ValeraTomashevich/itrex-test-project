package com.itrexgroup.itrexgrouptest.ui.movie;

import com.itrexgroup.itrexgrouptest.data.DataManager;
import com.itrexgroup.itrexgrouptest.ui.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class MoviePresenter<V extends MovieMvpView> extends BasePresenter<V> implements MovieMvpPresenter<V> {

    @Inject
    public MoviePresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }
}
