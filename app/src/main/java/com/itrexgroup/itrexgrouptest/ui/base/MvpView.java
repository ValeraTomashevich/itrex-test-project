package com.itrexgroup.itrexgrouptest.ui.base;

/**
 * MvpView: It is an interface, that is implemented by the View.
 * It contains methods that are exposed to its Presenter for the communication.
 */
public interface MvpView {

    void showLoading();

    void hideLoading();
}
