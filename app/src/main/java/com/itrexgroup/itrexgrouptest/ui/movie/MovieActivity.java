package com.itrexgroup.itrexgrouptest.ui.movie;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.itrexgroup.itrexgrouptest.ui.base.BaseActivity;

import javax.inject.Inject;

public class MovieActivity extends BaseActivity implements MovieMvpView {

    @Inject
    MovieMvpPresenter<MovieMvpView> presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivityComponent().inject(this);

        presenter.onAttach(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDetach();
    }
}
