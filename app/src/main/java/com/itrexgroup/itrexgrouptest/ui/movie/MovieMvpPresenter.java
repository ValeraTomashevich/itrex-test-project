package com.itrexgroup.itrexgrouptest.ui.movie;

import com.itrexgroup.itrexgrouptest.ui.base.MvpPresenter;

public interface MovieMvpPresenter<V extends MovieMvpView> extends MvpPresenter<V> {

}
